# Allgo.org Custom Wordpress Theme
A custom Wordpress site for the nonprofit [Allgo](http://allgo.org/) developed for the [OpenAir Competition](http://www.knowbility.org/v/open-air/).

## Community Provided Components
AKA, things to kinda know about when working with this site.

* [Wordpress](https://wordpress.org/)
* [Underscores Wordpress Starter Theme](http://underscores.me/)
* [Gulp](http://gulpjs.com/)
* [SASS](http://sass-lang.com/)
* [NPM](https://www.npmjs.org/)
* [git](http://git-scm.com/)
* [Font Awesome](http://fortawesome.github.io/Font-Awesome/)
* [Chrome LiveReload Extension](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en)

## Getting Started

1. Fork this repo into your own Bitbucket account. Clone it from your fork.
2. [Install Wordpress](http://codex.wordpress.org/Installing_WordPress) in the docroot folder of this repo. Core Wordpress components are restricted from being added to the repo in the [.gitignore](http://git-scm.com/docs/gitignore) file. 
3. Make sure you have [node.js](http://nodejs.org/) installed on your computer. Run __npm install__. This will install the project dependencies defined in the package.json file. These are primarily Gulp plugins used for development.
4. From the project root run Gulp during development. __gulp__

### Gulp
The gulpfile contains the following tasks:

1. __jshint__: This will watch for changes to Javascript files in the theme directory and prompt display detected errors in the command line.
2. __styles__: This will watch for changes to .scss files in the sass directory and process changes into the styles.css file in the theme directory.
3. __watch__: This task watches for the changes and runs the appropriate task on change. This task will also kick off the livereload.

### SASS
The SASS file have the following directory structure:

    |_ styles.scss
    |_ _partials
      |_ _blocks
      |_ _elements
      |_ _pages
      |_ _utilities

The styles.scss file is the main import point for all of the partial .scss files. The partials are divided into directories based on the following organizational structure:

* __elements__: Base styles for individual HTML elements. Example partial files that would live underneath this directory include:
    * _forms.scss - base styling for forms.
    * _icons.scss - base styling for icons.
    * _lists.scss - base styling for lists.
    * _tables.scss - base styling for tables.
    * _text.scss - base styling for text.
* __blocks__: Reusable blocks throughout the site. Examples include, navigation, header, sidebar widgets. Groups of elements form blocks.
* __pages__: Styles for individual pages. Examples would be styles particular to a home page or contact page.
* __utilities__: Helper styles. CSS libraries. A few example partials would be:
    * _mixins.scss: This partials is for SASS functions to include in other partials. Examples would be mixins for animation or media query breakpoints.
    * _reset.scss: CSS reset.
    * _variables.scss: Define variables reused throughout the rest of the .scss files. Examples would be colors, fonts, text-shadow, basically style that can be typed repetitively throughout the CSS.
    * _font_awesome.scss - Font Awesome!
    * _font-face.scss - Define font faces here.

The idea is to start defining styles are the smallest level, individual HTML elements, and progress through larger organization units, blocks into pages. 

### Development Server
The development server is on Digital Ocean. We don't control the DNS for this domain so you'll have to add this entry to your hosts file to see it

    104.236.28.51 dev.allgo.org
