<?php
/**
 * Plugin Name: Allgo Testimonials
 * Description: This plugin provides funcitonality to associate testimonials to pages.
 * Plugin URI: http://example.org/
 * Author: nofearinc
 * Author URI: http://devwp.eu/
 * Version: 1.5
 * Text Domain: dx-sample-plugin
 * License: GPL2
 */

/**
 * Get some constants ready for paths when your plugin grows 
 * 
 */

define( 'DXP_VERSION', '1.5' );
define( 'DXP_PATH', dirname( __FILE__ ) );
define( 'DXP_PATH_INCLUDES', dirname( __FILE__ ) . '/inc' );
define( 'DXP_FOLDER', basename( DXP_PATH ) );
define( 'DXP_URL', plugins_url() . '/' . DXP_FOLDER );
define( 'DXP_URL_INCLUDES', DXP_URL . '/inc' );


/**
 * 
 * The plugin base class - the root of all WP goods!
 * 
 * @author nofearinc
 *
 */
class AllgoTestimonials {
	
	/**
	 * 
	 * Assign everything as a call from within the constructor
	 */
	public function __construct() {
		
		// register meta boxes for Pages (could be replicated for posts and custom post types)
		add_action( 'add_meta_boxes', array( $this, 'allgo_testimonials_boxes_callback' ) );
		
		// register save_post hooks for saving the custom fields
		add_action( 'save_post', array( $this, 'allgo_save_testimonial_field' ) );
		
		// Add a sample shortcode
		add_action( 'init', array( $this, 'allgo_testimonials_shortcode' ) );
		
	}	
	
	/**
	 * 
	 *  Adding right and bottom meta boxes to Pages
	 *   
	 */
	public function allgo_testimonials_boxes_callback() {
		// register bottom box
		add_meta_box(
		  'dx_bottom_meta_box',
		  __( "Testimonials", 'dxbase' ), 
		  array( $this, 'allgo_testimonials_meta_box' ),
		  'page' // leave empty quotes as '' if you want it on all custom post add/edit screens or add a post type slug
		);
	}
	
	/**
	 * Save the custom field from the side metabox
	 * @param $post_id the current post ID
	 * @return post_id the post ID from the input arguments
	 * 
	 */
	public function allgo_save_testimonial_field( $post_id ) {
		$testimonials = array();
		// Avoid autosaves
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$slug = 'page';
		// If this isn't a 'book' post, don't update it.
    if ( ! isset( $_POST['post_type'] ) || $slug != $_POST['post_type'] ) {
			return;
		}
		
		// If the custom field is found, update the postmeta record
		if ( wp_verify_nonce( $_POST['allgo_testimonials_nonce'], 'allgo_testimonials' ) ) {
			foreach ($_POST as $post_key => $post_value) {
				if (strpos($post_key, 'allgo_testimonial_') === 0) {
					$key = substr($post_key, -1);
					if (strpos($post_key, 'allgo_testimonial_from') === 0 && $post_value) {
						$testimonials[$key]['from'] = $post_value;
					}
					if (strpos($post_key, 'allgo_testimonial_testimonial') === 0 && $post_value) {
						$testimonials[$key]['testimonial'] = $post_value;
					}
				}
			}
			update_post_meta( $post_id, 'allgo_testimonials', serialize($testimonials));
		} else {
			var_dump('bad nonce');
		}
	}
	
	public function allgo_testimonial_form_template($inc, $testimonial = NULL) {
		$from = '';
		$text = '';
		$number = $inc + 1;
		if ($testimonial) {
			$from = $testimonial['from'];
			$text = $testimonial['testimonial'];
		}
		$out = '<fieldset><legend>Testimonial #' . $number . '</legend><br />';
		$out .= '<label for="allgo_testimonial_from_' . $inc . '">Testimonial From: </label><br />';
		$out .= '<input type="text" id="allgo_testimonial_from_' . $inc . '" name="allgo_testimonial_from_' . $inc . '" class="regular-text" value="' . $from . '"/><br />';
		$out .= '<label for="allgo_testimonial_testimonial_' . $inc . '">Testimonial:</label><br />';
		$out .= '<textarea class="large-text" name="allgo_testimonial_testimonial_' . $inc . '" id="allgo_testimonial_testimonial_' . $inc . '">' . $text . '</textarea>';
		$out .= '</fieldset';
		return $out;
	}

	/**
	 * 
	 * Init bottom meta box here 
	 * @param post $post the post object of the given page 
	 * @param metabox $metabox metabox data
	 */
	public function allgo_testimonials_meta_box( $post, $metabox) {
		wp_nonce_field( 'allgo_testimonials', 'allgo_testimonials_nonce' );
		$testimonials = get_post_meta( $post->ID, 'allgo_testimonials', true );
		$i = 0;
		_e('<p>Enter testimonials for this page.</p>');
		if ($testimonials) {
			foreach (unserialize($testimonials) as $testimonial) {
				echo $this->allgo_testimonial_form_template($i, $testimonial);
				$i++;
			}
		}
		echo $this->allgo_testimonial_form_template($i);
	}
	
	/**
	 * Register a sample shortcode to be used
	 * 
	 * First parameter is the shortcode name, would be used like: [dxsampcode]
	 * 
	 */
	public function allgo_testimonials_shortcode() {
		add_shortcode( 'allgo_testimonials', array( $this, 'allgo_testimonials_shortcode_body' ) );
	}
	
	/**
	 * Returns the content of the sample shortcode, like [dxsamplcode]
	 * @param array $attr arguments passed to array, like [dxsamcode attr1="one" attr2="two"]
	 * @param string $content optional, could be used for a content to be wrapped, such as [dxsamcode]somecontnet[/dxsamcode]
	 */
	public function allgo_testimonials_shortcode_body( $attr, $content = null ) {
		/*
		 * Manage the attributes and the content as per your request and return the result
		 */
		global $post;
		$testimonials = get_post_meta( $post->ID, 'allgo_testimonials', TRUE );
		if ($testimonials) {
			$i = 0;
			$out = '<div class="testimonials">';
			foreach (unserialize($testimonials) as $testimonial) {
				$out .= '<aside class="testimonial" id="testimonial_' . $i .'">';
				$out .= '<p>' . $testimonial['testimonial'] . '</p>';
				$out .= '<p class="source">' . $testimonial['from'] . '</p>';
				$out .= '</aside>';
				$i++;
			}
			$out .= '</div>';
			return __( $out, 'dxbase');
		}
	}
	
}

// Initialize everything
$dx_plugin_base = new AllgoTestimonials();
