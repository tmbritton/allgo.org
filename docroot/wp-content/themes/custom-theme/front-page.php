<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Allgo
 */

get_header(); ?>

<?php get_template_part('content', 'features'); ?>

<!-- homepage events banner -->
<section id="next-event">

</section><!-- homepage events banner -->

<!-- bottom stuff -->
<div id="content" class="site-content">
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main"> 

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'content', 'page' ); ?>
      <?php endwhile; else: ?>
        <!-- Whoops! No posts to display! -->
      <?php endif; ?>

    </main><!-- #main -->
  </div><!-- #primary -->
<section class="subscribe clearfix" role="signup-for-mailinglist">

<div class="subscribe-content"><p><i class="fa fa-paper-plane fa-x2"></i>To join our mailing list, please enter your email address: </p>
 <form id="constant-contact-signup-1" onsubmit="return validate();"  action="/join-our-mailing-list/" method="post">  
      <label for="cc_5020312c46_email_address" class=" gfield_label screen-reader-text"> 
        Email Address (required)</label>
        <input type="text" required value="" placeholder="Email Address" name="fields[email_address][value]" class="t required" id="cc_5020312c46_email_address"> 
        <input type="hidden" name="fields[email_address][label]" value="Email Address"> 
        <input type="hidden" name="fields[email_address][req]" value="1"> 

        <input type="submit" value="Subscribe" class="b button" id="cc_5020312c46_Go" name="constant-contact-signup-submit"> 

      <input type="hidden" value="2" name="lists[]">
<input type="hidden" value="8" name="lists[]">
<input type="hidden" value="48" name="lists[]">

              <input type="hidden" id="cc_redirect_url" name="cc_redirect_url" value="">
              <input type="hidden" id="cc_referral_url" name="cc_referral_url" value="%2Fjoin-our-mailing-list%2F">
              <input type="hidden" name="cc_referral_post_id" value="257">
              <input type="hidden" name="uniqueformid" value="5020312c46">
              <input type="hidden" name="ccformid" value="1">
          </form>

</div>
</section>
  <div id="email-validation" class="error inactive">Sorry! The email address submitted was invalid. Please try again.</div>


  <div id="secondary" class="widget-area" role="complementary">
    <aside class="widget">
     
        <?php /* Widgetized sidebar */ if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Bottom') ) : ?><?php endif; ?>
    </aside>

    <aside class="widget">

        <?php /* Widgetized sidebar */ if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('BottomCenter') ) : ?><?php endif; ?>

    </aside>


    <aside class="widget">

        <?php /* Widgetized sidebar */ if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('BottomRight') ) : ?><?php endif; ?> 
    </aside>
  </div><!-- #secondary -->

  </div><!-- #content -->
</div>

<?php get_footer(); ?>
