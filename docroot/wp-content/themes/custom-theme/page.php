<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package custom-theme
 */

get_header(); ?>
<div id="content">
<div id="page-content">
  <?php 
    $before_crumb = '<p class="menu-breadcrumb"><a href="/">Home</a><span class="sep"> » </span>';
    if ( function_exists( 'menu_breadcrumb') ) { 
        menu_breadcrumb( 
            'primary',                             // Menu Location to use for breadcrumb
            ' &raquo; ',                        // separator between each breadcrumb
            $before_crumb,      // output before the breadcrumb
            '</p>'                              // output after the breadcrumb
        ); 
    } 
  ?>
  <?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>

	<?php endwhile; // end of the loop. ?>
</div>

<?php get_sidebar(); ?>
</div>
<section class="subscribe clearfix" role="signup-for-mailinglist">

<div class="subscribe-content"><p><i class="fa fa-paper-plane fa-x2"></i>To join our mailing list, please enter your email address: </p>
 <form id="constant-contact-signup-1" onsubmit="return validate();" action="/join-our-mailing-list/" method="post">  
      <label for="cc_5020312c46_email_address" class=" gfield_label screen-reader-text"> 
        Email Address (required)</label>
        <input type="text" required value="" placeholder="Email Address" name="fields[email_address][value]" class="t required" id="cc_5020312c46_email_address"> 
        <input type="hidden" name="fields[email_address][label]" value="Email Address"> 
        <input type="hidden" name="fields[email_address][req]" value="1"> 

        <input type="submit" value="Subscribe" class="b button" id="cc_5020312c46_Go" name="constant-contact-signup-submit"> 

      <input type="hidden" value="2" name="lists[]">
<input type="hidden" value="8" name="lists[]">
<input type="hidden" value="48" name="lists[]">

              <input type="hidden" id="cc_redirect_url" name="cc_redirect_url" value="">
              <input type="hidden" id="cc_referral_url" name="cc_referral_url" value="%2Fjoin-our-mailing-list%2F">
              <input type="hidden" name="cc_referral_post_id" value="257">
              <input type="hidden" name="uniqueformid" value="5020312c46">
              <input type="hidden" name="ccformid" value="1">
          </form>

</div>
</section>
  <div id="email-validation" class="error inactive">Sorry! The email address submitted was invalid. Please try again.</div>

</div>

<?php get_footer(); ?>