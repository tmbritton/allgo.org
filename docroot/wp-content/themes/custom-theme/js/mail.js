function validate() {
  var email = jQuery('#cc_5020312c46_email_address').val();
  console.log(email);
  if(!checkEmailPattern(email)) {
    var error = "Sorry! <span class=\"invalid-input\">" + email + "</span> is not a valid email address. Please enter a valid email address.";
    if (!/\S/.test(email)){
      error = "Please enter a valid email address";
    }
     jQuery('#email-validation').html(error).removeClass('inactive');
     jQuery('#cc_5020312c46_email_address').addClass('invalid');
     return false;    
  }
  
  /**
    * Check for valid email address
    *
    * @param string email
    * Text to check to see if it is valid email address
    *
    * @return boolean
    * true if is valid email address, false if is not a valid email address
    */
    function checkEmailPattern(email) {
      var re = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      return re.test(email);
    }

}

