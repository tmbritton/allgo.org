<?php
    
/**     
 * Allgo functions and definitions    
 *     
 * @package Allgo    
 */
    
if (!function_exists('allgo_setup')) {
    
  /**     
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */  
  function allgo_setup() {   
    /**
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.  
     * If you're building a theme based on Allgo, use a find and replace    
     * to change 'allgo' to the name of your theme in all the template files   
     */    
    load_theme_textdomain( 'allgo', get_template_directory() . '/languages' );
    // Add default posts and comments RSS feed links to head.    
    add_theme_support( 'automatic-feed-links' );

    /**     
     * Let WordPress manage the document title.    
     * By adding theme support, we declare that this theme does not use a    
     * hard-coded <title> tag in the document head, and expect WordPress to    
     * provide it for us.    
     */
    add_theme_support( 'title-tag' );
        
    /**  
     * Enable support for Post Thumbnails on posts and pages.    
     *    
     * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails   
     */     
    add_theme_support( 'post-thumbnails' );
            
    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
      'primary' => __( 'Primary Menu', 'allgo' ),                           
    ) );

    /**    
     * Switch default core markup for search form, comment form, and comments   
     * to output valid HTML5.    
     */
    add_theme_support( 'html5', array( 
     'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
    ) );
  }    
}
add_action( 'after_setup_theme', 'allgo_setup' );
    
    
    
/**
 * Register widget area.     
 *    
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function allgo_widgets_init() {  
  register_sidebar( array(
    'name'          => __( 'Sidebar-Top', 'allgo' ),                                
    'id'            => 'sidebar-top',
    'description'   => '',
    'before_widget' => '<aside id="%1$s" class="widget sidebar-top %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );      
  register_sidebar( array(
    'name'          => __( 'Sidebar', 'allgo' ),                                
    'id'            => 'sidebar-1',
    'description'   => '',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );
  register_sidebar(array(
    'name' => 'Bottom',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));
  
  register_sidebar(array(
    'name' => 'BottomCenter',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));        
      
  register_sidebar(array(
    'name' => 'BottomRight',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));      
}
add_action( 'widgets_init', 'allgo_widgets_init' );

/**     
 * Enqueue scripts and styles.    
 */
function allgo_scripts() {        
  wp_enqueue_style( 'allgo-style', get_stylesheet_uri() );
  wp_enqueue_script( 'allgo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
  wp_enqueue_script( 'allgo-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'allgo_scripts' );


/**
 * Register image sizes
 */
add_image_size( 'homepage_feature', 290, 395, array( 'center', 'center' ) );

/**     
 * Implement the Custom Header feature.  
 */
 //require get_template_directory() . '/inc/custom-header.php';
       
/**     
 * Custom template tags for this theme.  
 */   
require_once get_template_directory() . '/inc/template-tags.php';
        
/**     
 * Custom functions that act independently of the theme templates.    
 */   
require_once get_template_directory() . '/inc/extras.php';
    
/**     
 * Customizer additions.    
 */  
require_once get_template_directory() . '/inc/customizer.php';
      
/**     
 * Load Jetpack compatibility file.    
 */  
require_once get_template_directory() . '/inc/jetpack.php';
    
/**     
 * Adds the individual sections, settings, and controls to the theme customizer    
 */ 

function allgo_get_featured_posts() {
  $posts = array(
    0 => get_theme_mod('featured_page_one'),
    1 => get_theme_mod('featured_page_two'),
    2 => get_theme_mod('featured_page_three')
  );
  return $posts;
}

function allgo_get_state_array($abbr = NULL) {
  $states = array(
    'AL'=>'Alabama',
    'AK'=>'Alaska',
    'AZ'=>'Arizona',
    'AR'=>'Arkansas',
    'CA'=>'California',
    'CO'=>'Colorado',
    'CT'=>'Connecticut',
    'DE'=>'Delaware',
    'DC'=>'District of Columbia',
    'FL'=>'Florida',
    'GA'=>'Georgia',
    'HI'=>'Hawaii',
    'ID'=>'Idaho',
    'IL'=>'Illinois',
    'IN'=>'Indiana',
    'IA'=>'Iowa',
    'KS'=>'Kansas',
    'KY'=>'Kentucky',
    'LA'=>'Louisiana',
    'ME'=>'Maine',
    'MD'=>'Maryland',
    'MA'=>'Massachusetts',
    'MI'=>'Michigan',
    'MN'=>'Minnesota',
    'MS'=>'Mississippi',
    'MO'=>'Missouri',
    'MT'=>'Montana',
    'NE'=>'Nebraska',
    'NV'=>'Nevada',
    'NH'=>'New Hampshire',
    'NJ'=>'New Jersey',
    'NM'=>'New Mexico',
    'NY'=>'New York',
    'NC'=>'North Carolina',
    'ND'=>'North Dakota',
    'OH'=>'Ohio',
    'OK'=>'Oklahoma',
    'OR'=>'Oregon',
    'PA'=>'Pennsylvania',
    'RI'=>'Rhode Island',
    'SC'=>'South Carolina',
    'SD'=>'South Dakota',
    'TN'=>'Tennessee',
    'TX'=>'Texas',
    'UT'=>'Utah',
    'VT'=>'Vermont',
    'VA'=>'Virginia',
    'WA'=>'Washington',
    'WV'=>'West Virginia',
    'WI'=>'Wisconsin',
    'WY'=>'Wyoming',
  );
  if ($abbr) {
    return $states[$abbr];
  }
  else {
    return $states;
  }
} 

function allgo_get_contact_info() {
  $info = array();
  $info['street'] = get_theme_mod('allgo_street_address', '701 Tillery St.');
  $info['city'] = get_theme_mod('allgo_cityname', 'Austin');
  $info['state'] = get_theme_mod('allgo_state', 'TX');
  $info['zip'] = get_theme_mod('allgo_zipcode', '78702');
  $info['phone'] = get_theme_mod('allgo_phone', '(512) 472-2001');
  $info['email'] = get_theme_mod('allgo_email', 'allgo@allgo.org');
  $info['facebook'] = get_theme_mod('allgo_facebook', 'https://www.facebook.com/allgo.org');
  $info['twitter'] = get_theme_mod('allgo_twitter', 'https://twitter.com/algoqpoc');
  return $info;
}