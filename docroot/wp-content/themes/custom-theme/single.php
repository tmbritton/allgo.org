<?php
/**
 * The template for displaying all single posts.
 *
 * @package custom-theme
 */

get_header(); ?>

	<div>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

			<?php custom_theme_post_nav(); ?>

			<!-- <?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>

	</div>

<!-- <?php get_sidebar(); ?> 
<?php get_footer(); ?>
