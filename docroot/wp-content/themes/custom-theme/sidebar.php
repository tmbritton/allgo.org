<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package custom-theme
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="sidebar" class="widget-area" role="complementary">
  <?php dynamic_sidebar( 'sidebar-top' ); ?>
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->
