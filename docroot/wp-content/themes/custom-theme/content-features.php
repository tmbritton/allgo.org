<?php
/**
 * The template used for displaying featured pages on home page.
 *
 * @package Allgo
 */

$post_ids = allgo_get_featured_posts();
?>

<section id="home-features">
  <?php foreach($post_ids as $post_id) : ?>
    <?php 
      $featured_post = get_post($post_id, 'OBJECT'); 
      $url = get_permalink($post_id);
    ?>

<a href="<?php echo $url; ?>" id="<?php echo $featured_post->post_name; ?> " class="feature-block" 
  style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id($post_id), 'homepage_feature'); ?>
  )">


        
    
        <span class="link-text"><?php echo $featured_post->post_title; ?></span>
    </a><!-- .feature-block -->
  <?php endforeach; ?>
</section><!-- #home-features -->