<?php
/**
 * custom-theme Theme Customizer
 *
 * @package custom-theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function custom_theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
}
add_action( 'customize_register', 'custom_theme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function custom_theme_customize_preview_js() {
	wp_enqueue_script( 'custom_theme_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'custom_theme_customize_preview_js' );

/**     
 * Adds the individual sections, settings, and controls to the theme customizer    
 */   
function allgo_block_one( $wp_customize ) {
  $pages = get_pages();
  $page_options = array();
  
  foreach ($pages as $page) {
    $page_options[$page->ID] = $page->post_title;
  }
  
  $wp_customize->add_panel (
    'home_page',
    array(
    'priority'       => 10,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => 'Home Page',
    'description'    => 'Customization for home page features'
    )
  );
  $wp_customize->add_section(
    'block_one',
    array(
      'title' => 'Home Page Featured Pages',
      'description' => 'Select three pages to feature on home page',
      'priority' => 10,
      'panel' => 'home_page'
    )
  );
  $wp_customize->add_setting(
    'featured_page_one',
    array(
      'default' => '',
    )
  );
  $wp_customize->add_setting(
    'featured_page_two',
    array(
      'default' => '',
    )
  );
  $wp_customize->add_setting(
    'featured_page_three',
    array(
      'default' => '',
    )
  );
  $wp_customize->add_control(
    'featured_page_one',
    array(
      'label' => 'Featured Page in Block 1',
      'section' => 'block_one',
      'settings' => 'featured_page_one',
      'type' => 'select',
      'choices' => $page_options
    )
  );
  $wp_customize->add_control(
    'featured_page_two',
    array(
      'label' => 'Featured Page in Block 2',
      'section' => 'block_one',
      'settings' => 'featured_page_two',
      'type' => 'select',
      'choices' => $page_options
    )
  );
  $wp_customize->add_control(
    'featured_page_three',
    array(
      'label' => 'Featured Page in Block 3',
      'section' => 'block_one',
      'settings' => 'featured_page_three',
      'type' => 'select',
      'choices' => $page_options
    )
  );
}
add_action( 'customize_register', 'allgo_block_one' );

function allgo_contact_info ( $wp_customize ) {
  $wp_customize->add_panel (
    'allgo_contact_info',
    array(
    'priority'       => 15,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => 'Contact Info',
    'description'    => 'Enter Allgo contact info'
    )
  );
  $wp_customize->add_section(
    'allgo_address',
    array(
      'title' => 'Address',
      'description' => 'Enter Allgo street address',
      'panel' => 'allgo_contact_info',
      'priority' => 0
    )
  );
  $wp_customize->add_section(
    'allgo_phone_number',
    array(
      'title' => 'Phone Number',
      'description' => 'Enter Allgo phone number',
      'panel' => 'allgo_contact_info',
      'priority' => 1
    )
  );
  $wp_customize->add_section(
    'allgo_email_address',
    array(
      'title' => 'Email Address',
      'description' => 'Enter Allgo email address',
      'panel' => 'allgo_contact_info',
      'priority' => 2
    )
  );
  $wp_customize->add_section(
    'allgo_social_media',
    array(
      'title' => 'Social Media Accounts',
      'description' => 'URLs of social media accounts',
      'panel' => 'allgo_contact_info',
      'priority' => 3
    )
  );  
  $wp_customize->add_setting(
    'allgo_street_address',
    array(
      'default' => '701 Tillery St.',
    )
  );
  $wp_customize->add_control(
    'allgo_street_address',
    array(
      'label' => 'Street Address',
      'section' => 'allgo_address',
      'settings' => 'allgo_street_address',
      'type' => 'text'
    )
  );
  $wp_customize->add_setting(
    'allgo_cityname',
    array(
      'default' => 'Austin',
    )
  );
  $wp_customize->add_control(
    'allgo_cityname',
    array(
      'label' => 'City',
      'section' => 'allgo_address',
      'settings' => 'allgo_cityname',
      'type' => 'text'
    )
  );
  $wp_customize->add_setting(
    'allgo_state',
    array(
      'default' => 'TX',
    )
  );
  $wp_customize->add_control(
    'allgo_state',
    array(
      'label' => 'State',
      'section' => 'allgo_address',
      'settings' => 'allgo_state',
      'type' => 'select',
      'choices' => allgo_get_state_array()
    )
  );
  $wp_customize->add_setting(
    'allgo_zipcode',
    array(
      'default' => '78702',
    )
  );
  $wp_customize->add_control(
    'allgo_zipcode',
    array(
      'label' => 'Zipcode',
      'section' => 'allgo_address',
      'settings' => 'allgo_zipcode',
      'type' => 'text',
    )
  );
  $wp_customize->add_setting(
    'allgo_phone',
    array(
      'default' => '(512) 472-2001',
    )
  );
  $wp_customize->add_control(
    'allgo_phone',
    array(
      'label' => 'Phone Number',
      'section' => 'allgo_phone_number',
      'settings' => 'allgo_phone',
      'type' => 'text',
    )
  );
  $wp_customize->add_setting(
    'allgo_email',
    array(
      'default' => 'allgo@allgo.org',
    )
  );
  $wp_customize->add_control(
    'allgo_email',
    array(
      'label' => 'Email Address',
      'section' => 'allgo_email_address',
      'settings' => 'allgo_email',
      'type' => 'text',
    )
  );
  $wp_customize->add_setting(
    'allgo_facebook',
    array(
      'default' => 'https://www.facebook.com/allgo.org',
    )
  );  
  $wp_customize->add_control(
    'allgo_facebook',
    array(
      'label' => 'Facebook URL',
      'section' => 'allgo_social_media',
      'settings' => 'allgo_facebook',
      'type' => 'text',
    )
  );
  $wp_customize->add_setting(
    'allgo_twitter',
    array(
      'default' => 'https://twitter.com/algoqpoc',
    )
  );  
  $wp_customize->add_control(
    'allgo_twitter',
    array(
      'label' => 'Twitter URL',
      'section' => 'allgo_social_media',
      'settings' => 'allgo_twitter',
      'type' => 'text',
    )
  );
  $wp_customize->add_setting(
    'allgo_flickr',
    array(
      'default' => 'https://www.flickr.com/photos/allgo/',
    )
  );  
  $wp_customize->add_control(
    'allgo_flickr',
    array(
      'label' => 'Flickr URL',
      'section' => 'allgo_social_media',
      'settings' => 'allgo_flickr',
      'type' => 'text',
    )
  );  
}
add_action( 'customize_register', 'allgo_contact_info' );

