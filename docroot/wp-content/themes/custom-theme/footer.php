<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Allgo
 */
?>

<?php $contact = allgo_get_contact_info(); ?>

</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="wrap">
    <div class="site-info">
     <p>&copy; <?php echo date('Y'); ?> allgo</p>
      <address>
        <?php echo $contact['street']; ?> 
        <?php echo $contact['city']; ?>, 
        <?php echo $contact['state']; ?> 
        <?php echo $contact['zip']; ?>  | 
        <?php echo $contact['phone']; ?> | 
        <a href="mailto:<?php echo $contact['email']; ?>"><?php echo $contact['email']; ?></a>
      </address>
      <a href="/map-and-directions">Map and directions</a>
    </div><!-- .site-info -->
    
    <div class="social">
      <ul>
        <li><a target="_blank" href="<?php echo $contact['facebook']; ?>"><i class="fa fa-facebook"><span class="screen-reader-text">Facebook (opens in new window)</span></i></a></li>
        <li><a target="_blank" href="<?php echo $contact['twitter']; ?>"><i class="fa fa-twitter"><span class="screen-reader-text">Twitter (opens in new window)</span></i></a></li>
      </ul>
    </div><!-- .social -->
  </div><!-- .wrap -->
</footer><!-- #colophon -->

</div><!-- #page -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/mail.js"></script>

<?php wp_footer(); ?>

</body>
</html>