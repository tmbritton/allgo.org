<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package custom-theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<?php wp_head(); ?>
</head>
<body class="home blog">
<div id="page" class="hfeed site">
  <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

  <header id="masthead" class="site-header" role="banner">
  
    <nav id="site-navigation" class="main-navigation" role="navigation">
       <div class="wrap">
      <button class="menu-toggle" aria-controls="menu" aria-expanded="false"><i class="fa fa-bars fa-3x"><span class="screen-reader-text">Expand Menu</span></i></button>
     <ul id="menu" class="menu">
                <?php wp_nav_menu( array( 'items_wrap' => '%3$s', 'container' => '', 'depth' => '1' ) ); ?>
	    </ul>

<form role="search" method="get" class="search-form" action="/">
  <label for="search-field"><span class="screen-reader-text">Search for:</span></label>
          <input type="search" id="search-field" class="search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:" />
        <button type="submit"><i class="fa fa-search"><span class="screen-reader-text">Submit Search</span></i></button>
   </form>
    </div>
  </div>
</div>
    </nav><!-- #site-navigation -->
</div>
<div class="wrap">
    <div class="site-branding">
      <h1 class="site-title">
        <a href="<?php echo site_url(); ?>" rel="home"><img class="logo" src="<?php echo get_template_directory_uri()."/img/allgo-logo.png" ?>" alt="Allgo.org Home Page"/></a>
      </h1>
      <h2 class="site-description"><?php echo bloginfo('description'); ?></h2>
    </div><!-- .site-branding -->
  </div>



  </header><!-- #masthead -->

<div class="wrap">