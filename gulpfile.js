var gulp = require('gulp'),
    watch = require('gulp-watch'),
    notify = require('gulp-notify'),
    jshint = require('gulp-jshint'),
    plumber = require('gulp-plumber'),
    livereload = require('gulp-livereload'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    onError = function(err) {
      console.log(err);
    };

// Hint all of our custom developed Javascript to make sure things are clean
gulp.task('jshint', function() {
  return gulp.src('./docroot/wp-content/themes/custom-theme/js/**/*.js')
  .pipe(plumber({
    errorHandler: onError
  }))
  .pipe(jshint())
  .pipe(jshint.reporter('default'))
  .pipe(notify({ message: 'JS Hinting task complete' }));
});

gulp.task('styles', function() {
  gulp.src('./sass/**/**/*.scss')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./docroot/wp-content/themes/custom-theme'))
    .pipe(notify({ message: 'Stylesheets rebuilt' }))
    .pipe(livereload());
});

gulp.task('watch', function () {
  livereload.listen();
  // Whenever a stylesheet is changed, recompile
  gulp.watch('./sass/**/*.scss', ['styles']);

  // Whenever a javascript file is changed, run jshint
  gulp.watch('./docroot/wp-content/themes/custom-theme/js/**/*.js', ['jshint']).on('change', livereload.changed);

  //Whenever a template file is change, livereload
  gulp.watch('./docroot/wp-content/themes/custom-theme/**/*.php').on('change', livereload.changed);
});

gulp.task('default', ['styles', 'jshint', 'watch']);
